package main

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"flag"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
)

var (
	listenAddr string
	destURL    string
	certFile   string
	keyFile    string
	clientFile string
)

func init() {
	flag.StringVar(&listenAddr, "addr", ":443", "listen addr for ssl proxy")
	flag.StringVar(&destURL, "dest", "http://localhost:8080", "destnation hostname")
	flag.StringVar(&certFile, "cert", "cert.pem", "SSL/TLS certificate file")
	flag.StringVar(&keyFile, "key", "privkey.pem", "SSL/TLS private key file")
	flag.StringVar(&clientFile, "cakey", "cakey.pem", "SSL/TLS root certificate file for client authentication")
	flag.Parse()
}

func main() {
	destURL, err := url.Parse(destURL)
	if err != nil {
		log.Fatalf("failed to parse destination URL: %v", err)
	}

	certPool := x509.NewCertPool()
	{
		fh, err2 := os.Open(clientFile)
		if err2 != nil {
			log.Fatalf("failed to parse root certificate file: %v", err2)
		}
		defer fh.Close()

		buf := new(bytes.Buffer)
		rh := bufio.NewReader(fh)
		io.Copy(buf, rh)
		ok := certPool.AppendCertsFromPEM(buf.Bytes())
		if !ok {
			log.Fatalf("failed to append PEM file.")
		}
	}

	proxyHandler := httputil.NewSingleHostReverseProxy(destURL)
	proxyServer := http.Server{
		Addr:    listenAddr,
		Handler: proxyHandler,
		TLSConfig: &tls.Config{
			ClientAuth: tls.RequireAndVerifyClientCert,
			ClientCAs:  certPool,
		},
	}

	log.Fatal(proxyServer.ListenAndServeTLS(certFile, keyFile))
}
