sslr - Single host reverse proxy with SSL/TLS
=============================================

USAGE
-----

```bash
$ sslr -help
Usage of sslr:
  -addr=":443": listen addr for ssl proxy
  -cakey="cakey.pem": SSL/TLS root certificate file for client authentication
  -cert="cert.pem": SSL/TLS certificate file
  -dest="http://localhost:8080": destnation hostname
  -key="privkey.pem": SSL/TLS private key file
```

DESCRIPTION
-----------

`sslr` is a very simple single host reverse proxy with SSL/TLS.

This application is proxy to http server with SSL/TLS frontend.

And, this application was made for testing with SSL/TLS client authentication.

INSTALL
-------

```bash
$ go get github.com/nyarla/sslr
```

AUHTOR
------

Naoki OKAMURA (Nyarla) <nyarla@totep.net>

LICENSE
-------

[MIT](http://nyarla.mit-license.org/2014)

